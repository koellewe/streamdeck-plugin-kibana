/// <reference path="libs/js/action.js" />
/// <reference path="libs/js/stream-deck.js" />

const PERIOD_S = 5*60; // 5 mins
const INTERVAL_MS = 30*1000;

const myAction = new Action('za.co.rauten.streamdeck.kibana.action');

// dirty globals
let pluginSettings = null;
let pluginInterval = null;
//


myAction.onWillAppear(({context, payload}) => {
	pluginSettings = payload.settings;
	if (pluginInterval === null){
		$SD.logMessage('Starting interval');
		// Run immediately, then schedule
		getThatMPS(context);
		pluginInterval = setInterval(getThatMPS, INTERVAL_MS, context);
	}
});

myAction.onWillDisappear(({context}) => {
	$SD.logMessage('Killing interval');
	clearInterval(pluginInterval);
	pluginInterval = null;
});

async function createCanvasFromImage(url) {

	return await new Promise((resolve) => {
		var img = new Image();
		img.setAttribute('crossOrigin', 'anonymous');
	
		img.onload = function () {
			var canvas = document.createElement("canvas");
			canvas.width =this.width;
			canvas.height =this.height;
	
			var ctx = canvas.getContext("2d");
			ctx.drawImage(this, 0, 0);
	
			var dataURL = canvas.toDataURL("image/png");
	
			resolve(canvas);
		};
	
		img.src = url;
	});
}

function setTextOnImage(context, msgs){
	createCanvasFromImage("assets/kibana_logo.png").then(canvas => {
		
		const ctx = canvas.getContext("2d");
		// Write Messages count
		let textSize = Math.trunc(canvas.height / 3);
		ctx.font = "bold "+textSize+"px sans-serif";
		ctx.textAlign = "center";
		ctx.fillStyle = 'white';
		const mid_x = Math.trunc(canvas.width / 2);
		const high_y = Math.trunc(canvas.height * 0.55);

		ctx.fillText(msgs, mid_x, high_y, canvas.width);

		// Write label
		const low_y = high_y + textSize;
		textSize = canvas.height / 6;
		ctx.font = "bold "+textSize+"px sans-serif";
		ctx.fillText("msg / sec", mid_x, low_y, canvas.width);

		const data = canvas.toDataURL("image/png");
		$SD.logMessage("Setting custom image.");
		$SD.setImage(context, data);
	});
}

function getThatMPS(context){

	const http = new XMLHttpRequest();
	http.onreadystatechange = function() {
		if (http.readyState === XMLHttpRequest.DONE){
			if (http.status !== 200){
				$SD.logMessage("Got unexpected status from ElasticSearch: "+http.status);
				return;
			}

			const respData = JSON.parse(http.responseText);
			const msgs_period = respData['hits']['total']['value'];
			$SD.logMessage("Retrieved this number from ElasticSearch: "+msgs_period);
			const mps = msgs_period / PERIOD_S;
			
			setTextOnImage(context, mps.toFixed(1));
		}
	}

	http.open("POST", pluginSettings.elastic_host+"/_search?track_total_hits=true");
	http.setRequestHeader("kbn-xsrf", "true");
	http.setRequestHeader("Content-Type", "application/json");
	http.setRequestHeader("Authorization", "ApiKey "+pluginSettings.api_key);

	const now = new Date();
	const periodStart = new Date(now.getTime() - PERIOD_S*1000);
	http.send(JSON.stringify({
		"size": 0,
		"query": {
		  "bool": {
			"must": [
			  {
				"range": {
				  "@timestamp": {
					"gte": periodStart.toISOString(),
					"lte": now.toISOString(),
					"format": "strict_date_optional_time"
				  }
				}
			  },
			  {
				"bool": {
				  "must": [],
				  "filter": [],
				  "should": [],
				  "must_not": []
				}
			  }
			],
			"filter": JSON.parse(pluginSettings.json_filter_arr),
			"should": [],
			"must_not": []
		  }
		},
		"aggs": {},
		"runtime_mappings": {}
	  }));

	$SD.logMessage("Request sent.");
}

myAction.onDidReceiveSettings(({context, payload}) => {
	$SD.logMessage("Received settings: "+JSON.stringify(payload.settings));
	pluginSettings = payload.settings;
})

myAction.onKeyUp(({ action, context, device, event, payload }) => {
	$SD.logMessage("Click.");
});
