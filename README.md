# Stream Deck Kibana Plugin

Make requests to your own ElasticSearch instance! Right now, only events per second calculations are supported.

![Screenshot](repo_assets/sd_screenshot.jpg)

## Settings

- Elastic Host: The hostname of your ElasticSearch instance. Must include scheme and port (if not 80/443).
- Api Key: The Kibana API Key to be used to perform a search.
- JSON Filter Array: A JSON-formatted array containing your desired filters. Will be placed in the [filter context](https://www.elastic.co/guide/en/elasticsearch/reference/8.7/query-filter-context.html) of the query. Format should follow the [Elastic Query DSL](https://www.elastic.co/guide/en/elasticsearch/reference/8.7/query-dsl.html).
  - Use `[]` if you don't want to do any filtering.

## Contribution

More than welcome. Make issues and PRs to your heart's content.
